package org.example;



public class App {
    public static void main( String[] args ) {
      Singleton singletonIns =Singleton.getSingleton();
      singletonIns.message();

      FactoryService factory = new FactoryImpl();
      ProductService product = factory.getProduct();
      product.create();
    }
}
