package org.example;

public class FactoryImpl implements FactoryService{
    public ProductService getProduct() {
        return new ProductImpl();
    }
}
