package org.example;

public interface FactoryService {
    ProductService getProduct();
}
